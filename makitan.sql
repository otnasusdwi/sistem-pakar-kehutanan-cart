-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2017 at 10:23 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `makitan`
--

-- --------------------------------------------------------

--
-- Table structure for table `aturan_gejala`
--

CREATE TABLE IF NOT EXISTS `aturan_gejala` (
`kode_aturan_gejala` int(11) NOT NULL,
  `kode_kategori_tanaman` int(11) NOT NULL,
  `kode_pengganggu` int(11) NOT NULL,
  `kode_gejala` int(11) NOT NULL,
  `nilai_belief` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aturan_gejala`
--

INSERT INTO `aturan_gejala` (`kode_aturan_gejala`, `kode_kategori_tanaman`, `kode_pengganggu`, `kode_gejala`, `nilai_belief`, `foto`) VALUES
(1, 1, 2, 6, 50, 'G006-semai.jpg'),
(2, 1, 2, 7, 50, 'G007.jpg'),
(3, 1, 3, 8, 50, 'G008.jpg'),
(4, 1, 3, 9, 50, 'G009.JPG'),
(5, 1, 4, 10, 50, 'G010.jpg'),
(6, 1, 4, 11, 50, 'G011.jpg'),
(7, 1, 9, 1, 25, 'G001.jpg'),
(8, 1, 9, 4, 25, 'G004.jpg'),
(9, 1, 9, 5, 25, 'G005.jpg'),
(10, 1, 10, 1, 25, 'G001.jpg'),
(11, 1, 10, 4, 25, 'G004.jpg'),
(12, 1, 10, 5, 25, 'G005.jpg'),
(13, 2, 1, 1, 25, 'G001.jpg'),
(14, 2, 1, 2, 25, 'G002.jpg'),
(15, 2, 1, 3, 25, 'G003.jpg'),
(16, 2, 5, 12, 50, 'G012.jpg'),
(17, 2, 5, 13, 50, 'G013.jpg'),
(18, 2, 6, 14, 100, 'G014.jpg'),
(19, 2, 7, 15, 50, 'G015.jpg'),
(20, 2, 8, 16, 30, 'G016.jpg'),
(21, 2, 8, 17, 30, 'G017.jpg'),
(22, 2, 9, 1, 25, 'G001.jpg'),
(23, 2, 9, 4, 25, 'G004.jpg'),
(24, 2, 9, 5, 25, 'G005.jpg'),
(25, 2, 10, 1, 25, 'G001.jpg'),
(26, 2, 10, 4, 25, 'G004.jpg'),
(27, 2, 10, 5, 25, 'G005.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `aturan_tanda`
--

CREATE TABLE IF NOT EXISTS `aturan_tanda` (
`kode_aturan_tanda` int(11) NOT NULL,
  `kode_kategori_tanaman` int(11) NOT NULL,
  `kode_pengganggu` varchar(11) NOT NULL,
  `kode_tanda` varchar(11) NOT NULL,
  `nilai_belief` int(10) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aturan_tanda`
--

INSERT INTO `aturan_tanda` (`kode_aturan_tanda`, `kode_kategori_tanaman`, `kode_pengganggu`, `kode_tanda`, `nilai_belief`, `foto`) VALUES
(1, 1, '9', '4', 25, 'T004.jpg'),
(2, 1, '10', '5', 25, 'T005.jpg'),
(3, 2, '1', '1', 25, 'T001.jpg'),
(4, 2, '7', '2', 50, 'T002.jpg'),
(5, 2, '8', '3', 40, 'T003.jpg'),
(6, 2, '9', '4', 25, 'T004.jpg'),
(7, 2, '10', '5', 25, 'T005.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `gejala`
--

CREATE TABLE IF NOT EXISTS `gejala` (
`kode_gejala` int(11) NOT NULL,
  `nama_gejala` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gejala`
--

INSERT INTO `gejala` (`kode_gejala`, `nama_gejala`, `deskripsi`) VALUES
(1, 'Daun menguning', ''),
(2, 'Daun mengalami kelayuan', ''),
(3, 'Daun mengering dan akhirnya rontok', ''),
(4, 'Daun berlubang', ''),
(5, 'Daun mengering', ''),
(6, 'Pangkal batang busuk ', ''),
(7, 'Semai mati rebah ke tanah', ''),
(8, 'Permukaan daun terdapat bercak-bercak putih seperti embun tepung', ''),
(9, 'Ranting terdapat bercak-bercak putih seperti embun tepung', ''),
(10, 'Permukaan daun terdapat bercak-bercak berwarna hitam', ''),
(11, 'Ranting terdapat bercak-bercak berwarna hitam', ''),
(12, 'Batang membusuk', ''),
(13, 'Batang mengeluarkan cairan hitam', ''),
(14, 'Terdapat benjolan/pembengkakan pada bagian batang', ''),
(15, 'Batang membentuk alur-alur setelah dimakan rayap', ''),
(16, 'Batang berlubang-lubang', ''),
(17, 'Batang terdapat serbuk kayu', '');

-- --------------------------------------------------------

--
-- Table structure for table `histori`
--

CREATE TABLE IF NOT EXISTS `histori` (
`kode_histori` int(11) NOT NULL,
  `nilai_belief` varchar(50) NOT NULL,
  `tanggal` varchar(50) NOT NULL,
  `waktu` varchar(50) NOT NULL,
  `kode_user` varchar(50) NOT NULL,
  `kode_gejala` varchar(100) NOT NULL,
  `kode_tanda` varchar(50) NOT NULL,
  `kode_pengganggu` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `histori`
--

INSERT INTO `histori` (`kode_histori`, `nilai_belief`, `tanggal`, `waktu`, `kode_user`, `kode_gejala`, `kode_tanda`, `kode_pengganggu`) VALUES
(1, '', '23-07-2017', '21:58:12', 'susanto', 'Pangkal batang busuk , Semai mati rebah ke tanah', '', 'Damping-off'),
(2, '', '23-07-2017', '21:58:47', 'susanto', 'Pangkal batang busuk , Semai mati rebah ke tanah', 'Terdapat ulat kantong', 'Ulat kantong'),
(3, '', '23-07-2017', '21:59:51', 'susanto', 'Pangkal batang busuk , Semai mati rebah ke tanah', 'Terdapat ulat ', 'Ulat'),
(4, '', '23-07-2017', '22:00:25', 'susanto', 'Permukaan daun terdapat bercak-bercak putih sepert', '', 'Embun Tepung'),
(5, '', '23-07-2017', '22:00:43', 'susanto', 'Permukaan daun terdapat bercak-bercak berwarna hit', '', 'Embun Jelaga'),
(6, '', '23-07-2017', '22:01:08', 'susanto', 'Daun menguning, Daun berlubang, Daun mengering', '', 'Ulat kantong'),
(7, '', '23-07-2017', '22:01:40', 'susanto', 'Pangkal batang busuk , Semai mati rebah ke tanah', '', 'Damping-off'),
(8, '', '23-07-2017', '22:02:01', 'susanto', 'Permukaan daun terdapat bercak-bercak putih sepert', '', 'Embun Tepung'),
(9, '', '24-07-2017', '11:05:37', 'arifncahyo', 'Daun menguning, Daun berlubang, Daun mengering', 'Terdapat ulat kantong', 'Ulat kantong'),
(10, '', '24-07-2017', '11:06:11', 'arifncahyo', 'Daun menguning, Daun mengalami kelayuan, Daun meng', 'Munculnya tubuh buah', 'Busuk akar merah'),
(11, '', '24-07-2017', '11:07:19', 'alfin', 'Permukaan daun terdapat bercak-bercak putih sepert', '', 'Embun Tepung'),
(12, '', '24-07-2017', '11:07:50', 'alfin', 'Daun menguning, Daun berlubang, Daun mengering', 'Terdapat ulat ', 'Ulat'),
(13, '', '24-07-2017', '11:15:52', 'adlilia', 'Permukaan daun terdapat bercak-bercak berwarna hit', '', 'Embun Jelaga'),
(14, '', '24-07-2017', '11:17:05', 'adlilia', 'Pangkal batang busuk , Semai mati rebah ke tanah', '', 'Damping-off'),
(15, '', '24-07-2017', '11:21:00', 'fikri', 'Permukaan daun terdapat bercak-bercak putih sepert', '', 'Embun Tepung'),
(16, '', '24-07-2017', '11:21:49', 'dicky', 'Batang terdapat serbuk kayu', 'Terdapat larva (ulat) atau hama Xystrocera festiva', 'Penggerek Batang'),
(17, '', '24-07-2017', '11:22:29', 'dicky', 'Terdapat benjolan/pembengkakan pada bagian batang', '', 'Kanker batang'),
(18, '', '24-07-2017', '11:26:58', 'dodyirham', 'Permukaan daun terdapat bercak-bercak berwarna hit', '', 'Embun Jelaga'),
(19, '', '24-07-2017', '11:36:16', 'aisyahamini', 'Daun menguning, Daun mengalami kelayuan, Daun mengering dan akhirnya rontok', 'Munculnya tubuh buah', 'Busuk akar merah'),
(20, '', '24-07-2017', '11:36:34', 'aisyahamini', 'Batang membusuk, Batang mengeluarkan cairan hitam', '', 'Kanker hitam'),
(21, '', '24-07-2017', '11:40:37', 'mnis', 'Batang membentuk alur-alur setelah dimakan rayap', 'Terdapat rayap pada batang', 'Rayap'),
(22, '', '24-07-2017', '11:50:35', 'anggraeni', 'Daun menguning, Daun mengalami kelayuan', 'Munculnya tubuh buah', 'Busuk akar merah'),
(23, '', '24-07-2017', '11:51:11', 'anggraeni', 'Batang berlubang-lubang, Batang terdapat serbuk kayu', 'Terdapat larva (ulat) atau hama Xystrocera festiva', 'Penggerek Batang'),
(24, '', '24-07-2017', '11:52:59', 'anita', 'Batang membusuk, Batang mengeluarkan cairan hitam', '', 'Kanker hitam'),
(25, '', '24-07-2017', '11:53:13', 'anita', 'Daun menguning, Daun berlubang, Daun mengering', '', 'Ulat kantong'),
(26, '', '27-07-2017', '19:54:02', 'susanto', 'Daun menguning, Daun mengalami kelayuan, Daun mengering dan akhirnya rontok, Batang membusuk, Batang', 'Munculnya tubuh buah, Terdapat ulat kantong', 'Busuk akar merah'),
(27, '', '28-07-2017', '22:54:40', 'susanto', 'Daun menguning, Daun berlubang, Daun mengering', 'Terdapat ulat kantong, Terdapat ulat ', 'Ulat kantong'),
(28, '', '28-07-2017', '23:23:21', 'susanto', 'Daun menguning', 'Terdapat larva (ulat) atau hama Xystrocera festiva', 'Ulat kantong'),
(29, '', '02-08-2017', '22:37:32', 'susanto', 'Permukaan daun terdapat bercak-bercak putih seperti embun tepung', '', 'Embun Tepung'),
(30, '', '02-08-2017', '22:42:12', 'susanto', 'Pangkal batang busuk , Semai mati rebah ke tanah', '', 'Damping-off'),
(31, '', '02-08-2017', '23:04:45', 'susanto', 'Daun menguning, Daun mengering', 'Terdapat ulat ', 'Ulat'),
(32, '', '02-08-2017', '00:45:49', 'susanto', 'Semai mati rebah ke tanah', '', 'Damping-off'),
(33, '', '02-08-2017', '00:49:14', 'susanto', 'Daun menguning, Daun mengalami kelayuan, Daun mengering dan akhirnya rontok', 'Munculnya tubuh buah', 'Busuk akar merah'),
(34, '', '04-08-2017', '11:21:52', 'siti', 'Daun menguning, Daun mengalami kelayuan, Daun mengering dan akhirnya rontok', 'Munculnya tubuh buah', 'Busuk akar merah'),
(35, '', '04-08-2017', '14:56:56', 'susanto', 'Daun menguning, Daun berlubang', 'Terdapat ulat kantong', 'Ulat kantong'),
(36, '', '04-08-2017', '15:15:23', 'siti', 'Batang membusuk, Batang mengeluarkan cairan hitam', 'Terdapat larva (ulat) atau hama Xystrocera festiva', 'Kanker hitam');

-- --------------------------------------------------------

--
-- Table structure for table `jajal_gejala`
--

CREATE TABLE IF NOT EXISTS `jajal_gejala` (
`kode_aturan_gejala` int(11) NOT NULL,
  `kode_kategori_tanaman` int(11) NOT NULL,
  `kode_pengganggu` int(11) NOT NULL,
  `kode_gejala` int(11) NOT NULL,
  `nilai_belief` int(11) NOT NULL,
  `foto` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jajal_gejala`
--

INSERT INTO `jajal_gejala` (`kode_aturan_gejala`, `kode_kategori_tanaman`, `kode_pengganggu`, `kode_gejala`, `nilai_belief`, `foto`) VALUES
(1, 1, 3, 16, 0, ''),
(2, 1, 3, 9, 0, ''),
(3, 1, 6, 4, 0, ''),
(4, 1, 6, 2, 0, ''),
(5, 1, 6, 1, 0, ''),
(6, 2, 4, 9, 1111, ''),
(7, 2, 4, 1, 2222, ''),
(8, 2, 4, 16, 90, ''),
(9, 2, 4, 16, 222, ''),
(10, 2, 4, 16, 99, ''),
(11, 2, 4, 4, 1, ''),
(12, 2, 4, 6, 2, ''),
(13, 2, 4, 4, 1, ''),
(14, 2, 4, 6, 2, ''),
(15, 1, 5, 10, 1, ''),
(16, 1, 5, 7, 2, ''),
(17, 1, 5, 16, 1, ''),
(18, 1, 5, 16, 1, ''),
(19, 1, 1, 2, 1, 'Array'),
(20, 1, 1, 5, 2, 'Array'),
(21, 1, 1, 2, 1, 'akasia_embun jelaga 2.JPG'),
(22, 1, 1, 5, 2, 'akasia_kanker batang 2.jpg'),
(23, 1, 1, 2, 1, 'akasia_embun jelaga 2.JPG'),
(24, 1, 1, 5, 2, 'akasia_kanker batang 2.jpg'),
(25, 1, 1, 2, 1, 'akasia_embun jelaga 2.JPG'),
(26, 1, 1, 5, 2, 'akasia_kanker batang 2.jpg'),
(27, 1, 1, 2, 1, 'akasia_embun jelaga 2.JPG'),
(28, 1, 1, 5, 2, 'akasia_kanker batang 2.jpg'),
(29, 1, 1, 2, 1, 'akasia_embun jelaga 2.JPG'),
(30, 1, 1, 5, 2, 'akasia_kanker batang 2.jpg'),
(31, 1, 1, 2, 1, 'akasia_embun jelaga 2.JPG'),
(32, 1, 1, 5, 2, 'akasia_kanker batang 2.jpg'),
(33, 1, 1, 2, 1, 'akasia_embun jelaga 2.JPG'),
(34, 1, 1, 5, 2, 'akasia_kanker batang 2.jpg'),
(35, 1, 1, 16, 1, 'x.jpg'),
(36, 1, 1, 4, 2, 'y.jpg'),
(37, 1, 1, 16, 1, 'x.jpg'),
(38, 1, 1, 4, 2, 'y.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `jajal_tanda`
--

CREATE TABLE IF NOT EXISTS `jajal_tanda` (
  `kode_aturan_tanda` varchar(11) NOT NULL,
  `kode_kategori_tanaman` int(11) NOT NULL,
  `kode_pengganggu` int(11) NOT NULL,
  `kode_tanda` int(11) NOT NULL,
  `nilai_belief` int(11) NOT NULL,
  `foto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jajal_tanda`
--

INSERT INTO `jajal_tanda` (`kode_aturan_tanda`, `kode_kategori_tanaman`, `kode_pengganggu`, `kode_tanda`, `nilai_belief`, `foto`) VALUES
('0', 1, 5, 3, 1, ''),
('0', 1, 5, 5, 2, ''),
('', 1, 1, 2, 1, ''),
('', 1, 1, 2, 2, ''),
('', 1, 1, 2, 1, ''),
('', 1, 1, 2, 2, ''),
('', 1, 1, 2, 1, ''),
('', 1, 1, 2, 2, ''),
('', 1, 1, 2, 1, ''),
('', 1, 1, 2, 2, ''),
('', 1, 1, 2, 1, ''),
('', 1, 1, 2, 2, ''),
('', 1, 1, 2, 1, ''),
('', 1, 1, 2, 2, ''),
('', 1, 1, 2, 1, ''),
('', 1, 1, 2, 2, ''),
('', 1, 1, 2, 1, 'akasia_rayap.jpg'),
('', 1, 1, 2, 2, 'akasia_rayap.jpg'),
('', 1, 1, 1, 3, 'x.jpg'),
('', 1, 1, 1, 3, 'x.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_tanaman`
--

CREATE TABLE IF NOT EXISTS `jenis_tanaman` (
`kode_jenis_tanaman` int(11) NOT NULL,
  `nama_jenis_tanaman` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_tanaman`
--

INSERT INTO `jenis_tanaman` (`kode_jenis_tanaman`, `nama_jenis_tanaman`) VALUES
(1, 'Kehutanan'),
(2, 'Pertanian');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
`kode_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`kode_kategori`, `nama_kategori`) VALUES
(1, 'Semai'),
(2, 'Tegakan');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_pengganggu`
--

CREATE TABLE IF NOT EXISTS `kategori_pengganggu` (
`kode_kategori_pengganggu` int(11) NOT NULL,
  `nama_kategori_pengganggu` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_pengganggu`
--

INSERT INTO `kategori_pengganggu` (`kode_kategori_pengganggu`, `nama_kategori_pengganggu`) VALUES
(1, 'Hama'),
(2, 'Penyakit');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_tanaman`
--

CREATE TABLE IF NOT EXISTS `kategori_tanaman` (
`kode_kategori_tanaman` int(11) NOT NULL,
  `kode_tanaman` int(11) NOT NULL,
  `kode_kategori` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_tanaman`
--

INSERT INTO `kategori_tanaman` (`kode_kategori_tanaman`, `kode_tanaman`, `kode_kategori`, `foto`) VALUES
(1, 1, 1, 'akasia_semai.jpg'),
(2, 1, 2, 'akasia_tegakan.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `pengganggu`
--

CREATE TABLE IF NOT EXISTS `pengganggu` (
`kode_pengganggu` int(11) NOT NULL,
  `nama_pengganggu` varchar(100) NOT NULL,
  `nama_latin` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `kode_kategori_pengganggu` int(11) NOT NULL,
  `pengendalian` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengganggu`
--

INSERT INTO `pengganggu` (`kode_pengganggu`, `nama_pengganggu`, `nama_latin`, `foto`, `deskripsi`, `kode_kategori_pengganggu`, `pengendalian`) VALUES
(1, 'Busuk akar merah', 'Ganoderma philipii', 'P001.jpg', 'Penyakit busuk akar ini berlangsung secara perlahan dalam waktu yang lama, namun akibat yang ditimbulkannya sulit diperbaiki.', 2, 'Pembersihan tonggak dan sisa tanaman yang potensial sebagai sumber infeksi, pembuatan parit untuk mencegah penularan hifa patogen di tanah, dan penggunaan pestisida yang dirasa masih belum menunjukkan hasil yang efektif untuk skala pertanaman yang luas (Semangun, 1991a). Agen pengendali hayati yang sudah dikembangkan dewasa ini diantaranya adalah Trichoderma spp, Gliocladium spp, Pseudomonas fluorescens, Bacillus subtilis dan lain-lain. '),
(2, 'Damping-off', 'Damping-off', 'akasia_damping off 1.jpg', 'Damping-off merupakan penyakit pada kecambah atau tumbuhan yang masih sangat muda, yang umumnya terjadi bila lingkungan sangat lembab. Penyakit ini menyebabkan pembusukan akar dan batang yang masih sukulen atau bagian lain. Pembusukan diakibatkan oleh sejumlah fungi parasit fakultatif yang hidup di tanah dan tidak mempunyai inang yang tetap, diantaranya adalah: Fusarium sp., Pythium sp., Phytophthora sp., dan Rhizoctonia sp. (Boyce, 1961).', 2, 'Menggunakan agen pengendali hayati, seperti Trichoderma spp'),
(3, 'Embun Tepung', 'Powdery mildew', 'akasia_embun tepung 1.JPG', 'Penyakit embun tepung disebabkan oleh fungi Oidium sp. Dan sering ditemukan menyerang pada tanaman akasia. Penyakit embun tepung umumnya menyerang pada daun tanaman yang masih berusia muda. Permukaan daun yan terserang akan berwarna kuning, coklat atau hitam, dan akhirnya gugur. Hal tersebut akan memengaruhi proses fotosintesis sehingga pertumbuhan dan perkembangan tumbuhan menjadi terhambat. Infeksi pada daun muda akan menyebabkan adanya perubahan bentuk. ', 2, 'Pengaturan cahaya pada persemaian, mengurangi kondisi kelembaban tanah. Penggunaan fungisida. Melakukan eradikasi dan sanitasi pada tanah dan pupuk yang akan digunakan sebagai media tanam.'),
(4, 'Embun Jelaga', 'Black mildew', 'akasia_embun jelaga 1.JPG', 'Pada awalnya, jamur Capnodium sp. Akan membentuk lingkarang-lingkaran konsentris tipis dan akan melebar. Lingkaran tersebut lama-kelamaan akan menebal. Jamur ini tidak sampai masuk ke dalam jaringan tanaman, akan tetapi hanya menutupi permukaan daun. lapisan jamur tersebut akan menghambat proses fotosintesis pada tanaman. ', 2, 'Pengaturan cahaya pada persemaian, mengurangi kondisi kelembaban tanah. Melakukan pemangkasan pada daun yang sudah terserang embun jelaga. Pengaturan jarak tanam antar tanaman sehingga suplai sinar matahari mencukupi. Penggunaan fungisida. '),
(5, 'Kanker hitam', 'Pytopthora palmivora', 'akasia_kanker hitam 1.JPG', 'Kanker hitam umumnya menyerang pada tegakan akasia yang tumbuh pada tapak yang kurang subur dan kurang terawat. Pada awalnya, batang pohon terinfeksi jamur Pytopthora ketika terdapat luka pada batang. Jamur yang masuk akan menyerang pada kayu gubal dan menyebabkan batang menjadi busuk serta mengeluarkan cairan berwarna hitam.', 2, 'Melakukan monitoring pada lokasi yang kurang subur dan kurang terawat, menebang serta menyingkirkan pohon yang terserang kanker hitam, dan menggunakan fungisida.'),
(6, 'Kanker batang', 'Corticium salmonicolor', 'akasia_kanker batang 1.jpg', 'Kanker batang adalah penyakit yang menyerang batang pokok tanaman. Penyakit ini disebabkan oleh jamur yang perkembangannya sangat dipengaruhi oleh iklim mikro tegakan hutan. Patogen penyebab kanker lebih aktif pada daerah yang curah hujannya tinggi sehingga tanaman lebih rentan (OldÂ et al., 2000). Penyakit kanker berasosiasi dengan jamur upas (pink disease) yang disebabkan olehÂ Corticium salmonicolor (Hadi dan Simon, 1996).', 2, 'Pengendalian dapat dilakukan dengan menebang pohon yang telah terserang dan membersihkan semua tonggak dan sisa-sisa akar pohon yang terserang dan dimusnahkan. Pembuatan parit isolasi untuk mencegah penularan melalui kontak akar dari pohon yang terserang dengan pohon yang sehat.'),
(7, 'Rayap', 'Coptotermes curvignathus', 'akasia_rayap.jpg', 'Captotermes curvignathus memiliki kepala berwarna kuning, antena, lambrum dan pronotum berwarna kuning pucat. Rayap ini menyerang akasia dalam bentuk berkelompok. Umumnya rayap akan bersarang di atas atau di bawah tanah pada batang pohon. Rayap ini dapat menyerang kayu hingga pada bagian kayu teras.  Captotermes curvignathus biasa menyerang pada musim penghujan. ', 1, 'Pengendalian rayap umumnya dilakukan secara kimiawi dengan menggunakan insektisida yang diinjeksi pada bagian batang maupun disiramkan di sekitar akar pohon. '),
(8, 'Penggerek Batang', 'Xystrocera festiva', 'akasia_penggerek batang 1.jpg', 'Xystrocera festiva merupakan kumbang boktor yang termasuk ordo Coleoptera, berwarna merah kecoklatan, antena kehitaman, dan mempunyai 3 pasang kaki. Hama ini menyerang kayu akasia ketika fase larva. Larva akan menggerek kulit pada bagian dalam dan luar kayu. ', 1, 'Pengendalian dilakukan secara mekanik dengan membuat alat perangkap hama Xystrocera festiva. Pengendalian juga dapat dilakukan dengan pengaturan jarak antar tanaman, penanaman tanaman campuran, dan penjarangan tegakan secara berkala (Aprilia, 2011). '),
(9, 'Ulat kantong', 'Pteroma plagiophleps', 'akasia_ulat kantong.jpg', 'Ulat kantong termasuk dalam ordo Lepidotera, dengan tipe mulut pengunyah. Ulat kantong akan memakan daun dari arah bawah menuju ke atas, kemudian ulat ini akan melilit-lilitkan tubuhnya dengan daun hingga membentuk seperti kantong yang membungkus. Ulat kantong umumnya menyerang pada musim kemarau. ', 1, 'Ulat kantong dapat dikendalikan secara mekanik dengan mengambil ulat dari tanaman dan memusnahkannya. Secara kimiawi dilakukan dengan penggunaan insektisida bersifat sistemik yang diinfuskan atau disuntikkan ke dalam batang tanaman. Selain itu, dapat dilakukan pengendalian dengan musuh alami ulat kantong, yaitu : Nealsomyia rufella, Exorista psychidaum, dan beberapa nematoda entomophagus (Kalshoven, 1981 dalam Aprilia, 2011). '),
(10, 'Ulat', '', 'akasia_ulat 4.jpg', 'Ulat termasuk dalam ordo Lepidotera, dengan tipe mulut pengunyah. Ulat akan memakan daun sehingga membentuk lubang-lubang pada daun', 1, 'Apabila jumlah ulat tidak banyak, pengendalian dapat dilakukan dengan mengambil dan mematikan ulat.  Apabila tingkat serangan sudah tinggi, maka perlu dilakukan pengendalian dengan cara penyemprotan menggunakan insektisida.');

-- --------------------------------------------------------

--
-- Table structure for table `tanaman`
--

CREATE TABLE IF NOT EXISTS `tanaman` (
`kode_tanaman` int(11) NOT NULL,
  `nama_tanaman` varchar(50) NOT NULL,
  `nama_latin` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `kode_jenis_tanaman` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tanaman`
--

INSERT INTO `tanaman` (`kode_tanaman`, `nama_tanaman`, `nama_latin`, `deskripsi`, `kode_jenis_tanaman`) VALUES
(1, 'Akasia', 'Akasia', '', 1),
(2, 'Sengon', 'Paraserianthes Falcataria', '', 1),
(3, 'Pinus', 'Pinus Merkusii', '', 1),
(4, 'Jati', 'Tectona Grandis', '', 1),
(5, 'Kayu Putih', 'Melaleuca Cajuputi', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tanda`
--

CREATE TABLE IF NOT EXISTS `tanda` (
`kode_tanda` int(11) NOT NULL,
  `nama_tanda` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tanda`
--

INSERT INTO `tanda` (`kode_tanda`, `nama_tanda`, `deskripsi`) VALUES
(1, 'Munculnya tubuh buah', ''),
(2, 'Terdapat rayap pada batang', ''),
(3, 'Terdapat larva (ulat) atau hama Xystrocera festiva', ''),
(4, 'Terdapat ulat kantong', ''),
(5, 'Terdapat ulat ', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`kode_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `NIK` varchar(50) NOT NULL,
  `institusi` varchar(50) NOT NULL,
  `level` enum('pakar','pengguna') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`kode_user`, `username`, `email`, `password`, `alamat`, `NIK`, `institusi`, `level`) VALUES
(1, 'susanto', 'otnasusdwi@gmail.com', '12345', 'Jogja', '', '', 'pengguna'),
(2, 'pakar', 'pakar@gmail.com', '12345', 'Jogja', '', '', 'pakar'),
(3, 'dwi', 'dwi@gmail.com', '123', '', '', '', 'pengguna'),
(4, 'anggraeni', 'anggraenikusumadewi94@gmail.com', '212018', '', '', '', 'pengguna'),
(5, 'alfin', 'alfin@gmail.com', 'Alfin10', '', '', '', 'pengguna'),
(8, 'anita', 'newdewianita@gmail.com', '334016', '', '', '', 'pengguna'),
(10, 'dodyirham', 'irham.d.b@gmail.com', 'DOdiiYosh', '', '', '', 'pengguna'),
(11, 'aisyahamini', 'aisyahamini17@gmail.com', '12345', '', '', '', 'pengguna'),
(12, 'dicky', 'bigdick.ofponcol@gmail.com', 'BIGDICK69', '', '', '', 'pengguna'),
(15, 'fikri', 'fikri.ardiansyah@gmail.com', '12345', '', '', '', 'pengguna'),
(16, 'adlilia', 'adlilia@gmail.com', 'qwerty', '', '', '', 'pengguna'),
(17, 'mnis', 'muhammadnis@gmail.com', '12345', '', '', '', 'pengguna'),
(18, 'arifncahyo', 'nurcahyoa9@gmail.com', 'katasandi', '', '', '', 'pengguna'),
(19, 'siti', 'siti@gmail.com', '12345', 'Jogja', '12345', 'UAD', 'pengguna');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aturan_gejala`
--
ALTER TABLE `aturan_gejala`
 ADD PRIMARY KEY (`kode_aturan_gejala`);

--
-- Indexes for table `aturan_tanda`
--
ALTER TABLE `aturan_tanda`
 ADD PRIMARY KEY (`kode_aturan_tanda`);

--
-- Indexes for table `gejala`
--
ALTER TABLE `gejala`
 ADD PRIMARY KEY (`kode_gejala`);

--
-- Indexes for table `histori`
--
ALTER TABLE `histori`
 ADD PRIMARY KEY (`kode_histori`);

--
-- Indexes for table `jajal_gejala`
--
ALTER TABLE `jajal_gejala`
 ADD PRIMARY KEY (`kode_aturan_gejala`);

--
-- Indexes for table `jenis_tanaman`
--
ALTER TABLE `jenis_tanaman`
 ADD PRIMARY KEY (`kode_jenis_tanaman`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
 ADD PRIMARY KEY (`kode_kategori`);

--
-- Indexes for table `kategori_pengganggu`
--
ALTER TABLE `kategori_pengganggu`
 ADD PRIMARY KEY (`kode_kategori_pengganggu`);

--
-- Indexes for table `kategori_tanaman`
--
ALTER TABLE `kategori_tanaman`
 ADD PRIMARY KEY (`kode_kategori_tanaman`);

--
-- Indexes for table `pengganggu`
--
ALTER TABLE `pengganggu`
 ADD PRIMARY KEY (`kode_pengganggu`);

--
-- Indexes for table `tanaman`
--
ALTER TABLE `tanaman`
 ADD PRIMARY KEY (`kode_tanaman`);

--
-- Indexes for table `tanda`
--
ALTER TABLE `tanda`
 ADD PRIMARY KEY (`kode_tanda`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`kode_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aturan_gejala`
--
ALTER TABLE `aturan_gejala`
MODIFY `kode_aturan_gejala` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `aturan_tanda`
--
ALTER TABLE `aturan_tanda`
MODIFY `kode_aturan_tanda` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `gejala`
--
ALTER TABLE `gejala`
MODIFY `kode_gejala` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `histori`
--
ALTER TABLE `histori`
MODIFY `kode_histori` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `jajal_gejala`
--
ALTER TABLE `jajal_gejala`
MODIFY `kode_aturan_gejala` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `jenis_tanaman`
--
ALTER TABLE `jenis_tanaman`
MODIFY `kode_jenis_tanaman` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
MODIFY `kode_kategori` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kategori_pengganggu`
--
ALTER TABLE `kategori_pengganggu`
MODIFY `kode_kategori_pengganggu` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kategori_tanaman`
--
ALTER TABLE `kategori_tanaman`
MODIFY `kode_kategori_tanaman` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pengganggu`
--
ALTER TABLE `pengganggu`
MODIFY `kode_pengganggu` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tanaman`
--
ALTER TABLE `tanaman`
MODIFY `kode_tanaman` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tanda`
--
ALTER TABLE `tanda`
MODIFY `kode_tanda` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `kode_user` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
