<?php
	include ("../lib/koneksi.php");
	include ("template/head.php");
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pakar') {
    	header("Location: ../");
    }
    else{
	include ("template/topbar.php");
	include ("template/sidebar.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<section class="content-header">
		<h1>Tambah Jenis Tanaman</h1>
		<ol class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li><a href="tanaman.php">Jenis Tanaman</a></li>
			<li class="active">Tambah Jenis Tanaman</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<form name="tambah" role="form" action="add_jenistanamanproses.php" method="post" >               
						<div class="box-body">
							<!-- text input -->
							<div class="form-group">
								<label>Nama Jenis Tanaman</label>
								<input type="text" class="form-control" name="nama_jenis_tanaman" placeholder="Nama Jenis Tanaman ...">
							</div>
						</div>
						<!-- /.box-body -->	
						<div class="box-footer">
							<button type="reset" class="btn btn-default" ><a href="jenis_tanaman.php">Batal</a></button>
							<button type="submit" name="tambah" value="tambah" class="btn btn-primary">Tambah</button>
						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>