<?php
	include ("../lib/koneksi.php");
	include ("template/head.php");
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pakar') {
    	header("Location: ../");
    }
    else{
	include ("template/topbar.php");
	include ("template/sidebar.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<?php 
		if(isset($_GET['pesan'])){
			$pesan = $_GET['pesan'];
			if($pesan == "input"){
				echo "Data berhasil di input.";
			}else if($pesan == "gagal"){
				echo "Data gagal di input.";
			}else if($pesan == "update"){
				echo "Data berhasil di update.";
			}else if($pesan == "gagalupdate"){
				echo "Data berhasil gagal di update.";
			}else if($pesan == "hapus"){
				echo "Data berhasil di hapus.";
			}else if($pesan == "gagalhapus"){
				echo "Data berhasil gagal di hapus.";
			}
		}
	?>
	<section class="content-header">
		<h1>Tambah Kategori Pengganggu</h1>
		<ol class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li><a href="kategori_pengganggu.php">Kategori Pengganggu</a></li>
			<li class="active">Tambah Kategori Pengganggu</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<form name="tambah" role="form" action="add_kategoripenggangguproses.php" method="post" >
						<!-- text input -->
						<div class="box-body">														
							<div class="form-group">
								<label>Nama Kategori</label>
								<input type="text" class="form-control" name="nama_kategori_pengganggu" placeholder="Nama Kategori Pengganggu ...">
							</div>
						</div>
						<!-- /.box-body-->

						<div class="box-footer">
							<button type="reset" class="btn btn-default" ><a href="kategori_pengganggu.php">Batal</a></button>
							<button type="submit" name="tambah" value="tambah" class="btn btn-primary">Tambah</button>
						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>

<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>