<?php
	include ("../lib/koneksi.php");
	include ("template/head.php");
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pakar') {
    	header("Location: ../");
    }
    else{
	include ("template/topbar.php");
	include ("template/sidebar.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<section class="content-header">
		<h1>Tambah Kategori Tanaman</h1>
		<ol class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li><a href="kategori_tanaman.php">Kategori Tanaman</a></li>
			<li class="active">Tambah Kategori Tanaman</li>
	  	</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">               
					<form name="tambah" role="form" action="add_kategoritanamanproses.php" method="post" enctype="multipart/form-data">
						<div class="box-body">
							<div class="form-group">
								<label>Nama Tanaman</label>
								<select class="form-control select2" style="width: 100%;" name="kode_tanaman">
									<?php 
										$sql = mysql_query('SELECT * FROM tanaman ORDER BY nama_tanaman ASC;');
										if (mysql_num_rows($sql)>0) { ?>
											<?php while ($row = mysql_fetch_array($sql)) { ?>
												<option value="<?php echo $row['kode_tanaman'] ?>"><?php echo $row['nama_tanaman'] ?></option>}
										<?php	} ?>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<label>Kategori</label>
								<select class="form-control select2" style="width: 100%;" name="kode_kategori">
									<?php 
										$sql = mysql_query('SELECT * FROM kategori ORDER BY nama_kategori ASC;');
										if (mysql_num_rows($sql)>0) { ?>
											<?php while ($row = mysql_fetch_array($sql)) { ?>
												<option value="<?php echo $row['kode_kategori'] ?>"><?php echo $row['nama_kategori'] ?></option>}
										<?php	} ?>
									<?php } ?>
								</select>
								<input type="file" name="foto">
							</div>
						</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="reset" class="btn btn-default" ><a href="kategori_tanaman.php">Batal</a></button>
							<button type="submit" name="tambah" value="tambah" class="btn btn-primary">Tambah</button>
						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>

<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>