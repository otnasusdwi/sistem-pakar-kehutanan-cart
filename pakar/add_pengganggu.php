<?php
	include ("../lib/koneksi.php");
	include ("template/head.php");
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pakar') {
    	header("Location: ../");
    }
    else{
	include ("template/topbar.php");
	include ("template/sidebar.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
    	<h1>Tambah Pengganggu</h1>
	    <ol class="breadcrumb">
    	    <li><a href="index.php">Home</a></li>
        	<li><a href="pengganggu.php">Pengganggu</a></li>
        	<li class="active">Tambah Pengganggu</li>
      	</ol>
    </section>
    <!-- Main content -->
    <section class="content">
    	<div class="row">
            <div class="col-xs-12">
        	    <div class="box">
	              	<form name="tambah" role="form" action="add_penggangguproses.php" method="post" enctype="multipart/form-data">
		                <!-- text input -->
		                <div class="box-body">
			                <div class="form-group">
			                  <label>Nama Pengganggu</label>
			                  <input type="text" class="form-control" name="nama_pengganggu" placeholder="Nama Pengganggu ...">
			                </div>

			                <div class="form-group">
			                  <label>Nama Latin</label>
			                  <input type="text" class="form-control" name="nama_latin" placeholder="Nama Latin ...">
			                </div>

			                <div class="form-group">
			                  <label>Foto Pengganggu</label>
		                  		<input type="file" name="gambar">
			                </div>

			                <div class="form-group">
			                	<label>Kategori Pengganggu</label>
				                <select class="form-control select2" style="width: 100%;" name="kode_kategori_pengganggu">
				                	<?php 
					                		$sql = mysql_query('SELECT * FROM kategori_pengganggu ORDER BY nama_kategori_pengganggu ASC;');
					                		if (mysql_num_rows($sql)>0) { ?>
					                			<?php while ($row = mysql_fetch_array($sql)) { ?>
					                				<option value="<?php echo $row['kode_kategori_pengganggu'] ?>"><?php echo $row['nama_kategori_pengganggu'] ?></option>}
					                	<?php	} ?>
					                <?php } ?>
					            </select>
				             </div>

			                <!-- textarea -->
			                <div class="form-group">
			                  <label>Deskripsi</label>
			                  <textarea class="form-control" rows="3" name="deskripsi" placeholder="Deskripsi ..."></textarea>
			                </div>

			                <!-- textarea -->
			                <div class="form-group">
			                  <label>Pengendalian</label>
			                  <textarea class="form-control" rows="3" name="pengendalian" placeholder="Pengendalian ..."></textarea>
			                </div>
			            </div>
	            		<!-- /.box-body -->

		                <div class="box-footer">
			                <button type="reset" class="btn btn-default" ><a href="pengganggu.php">Batal</a></button>
			                <button type="submit" name="tambah" value="tambah" class="btn btn-primary">Tambah</button>
			            </div>
	            	</form>
	            </div>
	            <!-- /.box -->
        	</div>
    	</div>
    </section>
    <!-- /.content -->
</div>

<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>