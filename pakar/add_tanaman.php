<?php
   include ("../lib/koneksi.php");
	include ("template/head.php");
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pakar') {
    	header("Location: ../");
    }
    else{
	include ("template/topbar.php");
	include ("template/sidebar.php");
?>

<div class="content-wrapper">
	<section class="content-header">
		<h1>Tambah Tanaman</h1>
		<ol class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li><a href="tanaman.php">Tanaman</a></li>
			<li class="active">Tambah Tanaman</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">               
					<form name="tambah" role="form" action="add_tanamanproses.php" method="post" >
						<div class="box-body">
							<div class="form-group">
								<label>Nama Tanaman</label>
								<input type="text" class="form-control" name="nama_tanaman" placeholder="Nama Tanaman ...">
							</div>

							<div class="form-group">
								<label>Nama Latin</label>
								<input type="text" class="form-control" name="nama_latin" placeholder="Nama Latin ...">
							</div>

							<!-- textarea -->
							<div class="form-group">
								<label>Deskripsi</label>
								<textarea class="form-control" rows="3" name="deskripsi" placeholder="Deskripsi ..."></textarea>
							</div>

							<div class="form-group">
								<label>Jenis Tanaman</label>
								<select class="form-control select2" style="width: 100%;" name="jenis_tanaman">
									<?php 
										$sql = mysql_query('SELECT * FROM jenis_tanaman ORDER BY nama_jenis_tanaman ASC;');
										if (mysql_num_rows($sql)>0) { ?>
											<?php while ($row = mysql_fetch_array($sql)) { ?>
												<option value="<?php echo $row['kode_jenis_tanaman'] ?>"><?php echo $row['nama_jenis_tanaman'] ?></option>}
										<?php	} ?>
									<?php } ?>
								</select>
							</div>

						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<button type="reset" class="btn btn-default" ><a href="tanaman.php">Batal</a></button>
							<button type="submit" name="tambah" value="tambah" class="btn btn-primary">Tambah</button>
						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>