<?php
	include ("../lib/koneksi.php");
	include ("template/head.php");
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pakar') {
    	header("Location: ../");
    }
    else{
	include ("template/topbar.php");
	include ("template/sidebar.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<?php 
		if(isset($_GET['pesan'])){
			$pesan = $_GET['pesan'];
			if($pesan == "input"){
				echo "Data berhasil di input.";
			}else if($pesan == "gagal"){
				echo "Data gagal di input.";
			}else if($pesan == "update"){
				echo "Data berhasil di update.";
			}else if($pesan == "gagalupdate"){
				echo "Data berhasil gagal di update.";
			}else if($pesan == "hapus"){
				echo "Data berhasil di hapus.";
			}else if($pesan == "gagalhapus"){
				echo "Data berhasil gagal di hapus.";
			}
		}
	?>
	<section class="content-header">
		<h1>Data Aturan</h1>
		<ol class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li class="active">Aturan</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="add_aturan.php" class="btn btn-primary">Tambah Aturan</a>  
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="data" class="table table-striped table-bordered">
							<thead>

								<tr>
									<th width="5%">No</th>
									<th width="10%">Nama Tanaman</th>
									<th width="10%">Kategori</th>
									<th width="10%">Pengganggu</th>
									<th width="25%">Gejala</th>
									<th width="25%">Foto</th>
									<th width="15%">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$i = 1;
									$sql = mysql_query("SELECT * FROM aturan_gejala ORDER BY kode_aturan_gejala DESC")or die(mysql_error());
									while ($data = mysql_fetch_array($sql)) {
										$kode_kategori_tanaman = $data['kode_kategori_tanaman'];
										$kode_pengganggu = $data['kode_pengganggu'];
										$kode_gejala = $data['kode_gejala'];
										$sql1 = mysql_query("SELECT nama_pengganggu FROM pengganggu WHERE kode_pengganggu = $kode_pengganggu")or die(mysql_error());
										while ($data1 = mysql_fetch_array($sql1)) {
											$sql2 = mysql_query("SELECT kode_tanaman,kode_kategori FROM kategori_tanaman WHERE kode_kategori_tanaman = $kode_kategori_tanaman")or die(mysql_error());
											while ($data2 = mysql_fetch_array($sql2)) {
												$kode_tanaman = $data2['kode_tanaman'];
												$kode_kategori = $data2['kode_kategori'];
												$sql3 = mysql_query("SELECT nama_tanaman FROM tanaman WHERE kode_tanaman = $kode_tanaman")or die(mysql_error());
												while ($data3 = mysql_fetch_array($sql3)) {
													$sql4 = mysql_query("SELECT nama_kategori FROM kategori WHERE kode_kategori = $kode_kategori")or die(mysql_error());
													while ($data4 = mysql_fetch_array($sql4)) {
														$sql5 = mysql_query("SELECT nama_gejala FROM gejala WHERE kode_gejala = $kode_gejala")or die(mysql_error());
															while ($data5 = mysql_fetch_array($sql5)) {
								 ?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $data3['nama_tanaman']; ?></td>
									<td><?php echo $data4['nama_kategori']; ?></td>
									<td><?php echo $data1['nama_pengganggu']; ?></td>
									<td><?php echo $data5['nama_gejala']; ?></td>
									<td><img class="img-rounded" alt="Cinque Terre" width="152" height="118" img src="../gambar/gejala/<?php echo $data['foto']; ?>"></td>
										
									<td>
										<a href="edit_aturangejala.php?id=<?php echo $data['kode_aturan_gejala']; ?>" class="btn btn-primary btn-sm" > Edit </a> 
										<a href="delete_aturangejala.php?id=<?php echo $data['kode_aturan_gejala']; ?>" class="btn btn-warning btn-sm"> Hapus </a>
									</td>
								</tr>
								<?php 			
														}
													}
												}
											}
										}
										$i++;
									}
								 ?>
							</tbody>
						</table>
						<div class="modal fade" id="foto_gjl" role="dialog">
						        <div class="modal-dialog" role="document">
						            <div class="modal-content">
						                <div class="modal-header">
						                    <button type="button" class="close" data-dismiss="modal">&times;</button>
						                    <h4 class="modal-title">Foto Gejala</h4>
						                </div>
						                <div class="modal-body">
						                    <div class="hasil-data"></div>
						                </div>
						                <div class="modal-footer">
						                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
						                </div>
						            </div>
						        </div>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>