<?php
	include ("../lib/koneksi.php");
	include ("template/head.php");
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pakar') {
    	header("Location: ../");
    }
    else{
	include ("template/topbar.php");
	include ("template/sidebar.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
    	<h1>Edit Aturan Tanda</h1>
    	<ol class="breadcrumb">
        	<li><a href="index.php">Home</a></li>
        	<li><a href="aturan_tanda.php">Aturan Tanda</a></li>
        	<li class="active">Aturan Tanda</li>
      	</ol>
    </section>
    <!-- Main content -->
    <section class="content">
    	<div class="row">
            <div class="col-xs-12">
            	<div class="box">
	                <?php 
						$kode_aturan_tanda = $_GET['id'];
						$query_mysql = mysql_query("SELECT * FROM aturan_tanda WHERE kode_aturan_tanda='$kode_aturan_tanda'")or die(mysql_error());
						while($data = mysql_fetch_array($query_mysql)){
							$kode_tanda = $data['kode_tanda'];
							
					?>
	            	<form name="update" role="form" action="edit_aturantandaproses.php" method="post" enctype="multipart/form-data">
	                <!-- text input -->
	                	<div class="box-body">
	                		<div class="form-group">
			                	<label>Nama Tanda</label>
			                	<input type="hidden" name="kode_aturan_tanda" value="<?php echo $data['kode_aturan_tanda'] ?>">
				                <select class="form-control select2" style="width: 100%;" name="kode_tanda" >
				                	<?php 
					               		$sql = mysql_query('SELECT * FROM tanda ORDER BY nama_tanda ASC;');
					               		if (mysql_num_rows($sql)>0) { ?>
					               			<?php while ($row = mysql_fetch_array($sql)) { ?>
					               				<option value="<?php echo $row['kode_tanda'] ?>"><?php echo $row['nama_tanda'] ?></option>}
					                	<?php } ?>
					                <?php } ?>
					            </select>
				            </div>

			                <div class="form-group">
			                	<label>Foto Tanda</label><br>
			                  	<img src="../gambar/tanda/<?php echo $data['foto']; ?>" width="100"> <br>
		                  		<input type="file" name="gambar" value="<?php echo $data['foto'] ?>">
			                </div>
			
							<div class="form-group">
								<label>Nilai Belief</label>
								<input type="text" class="form-control" name="nilai_belief" value="<?php echo $data['nilai_belief'] ?>">
							</div>
						</div>

			            </div>
            			<!-- /.box-body -->    
		                <div class="box-footer">
			                <button type="reset" class="btn btn-default" ><a href="aturan_tanda.php">Batal</a></button>
			                <button type="submit" name="update" value="update" class="btn btn-primary">Update</button>
			            </div>
	            	</form>
	               	<?php } ?>
            	</div>
              	<!-- /.box -->
            </div>
    	</div>
    </section>
    <!-- /.content -->
</div>

<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>