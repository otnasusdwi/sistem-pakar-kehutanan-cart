<?php
	include ("../lib/koneksi.php");
	include ("template/head.php");
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pakar') {
    	header("Location: ../");
    }
    else{
	include ("template/topbar.php");
	include ("template/sidebar.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<section class="content-header">
		<h1>Edit Kategori</h1>
	  	<ol class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li><a href="kategori.php">Kategori</a></li>
			<li class="active">Edit Kategori</li>
	  	</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<?php 
						$id = $_GET['id'];
						$query = mysql_query("SELECT * FROM kategori WHERE kode_kategori='$id'")or die(mysql_error());
						while($data = mysql_fetch_array($query)){
					?> 
					<form name="update" role="form" action="edit_kategoriproses.php" method="post" >
						<div class="box-body"> 
							<div class="form-group">
						  		<label>Nama Kategori</label>
						  		<input type="hidden" name="kode_kategori" value="<?php echo $data['kode_kategori'] ?>">
						  		<input type="text" class="form-control" name="nama_kategori" value="<?php echo $data['nama_kategori'] ?>">
							</div>
						</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="reset" class="btn btn-default" ><a href="kategori.php">Batal</a></button>
							<button type="submit" name="update" value="update" class="btn btn-primary">Update</button>
						</div>
					</form>
					<?php
						}
					?>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>