<?php
  include ("../lib/koneksi.php");
	include ("template/head.php");
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pakar') {
    	header("Location: ../");
    }
    else{
	include ("template/topbar.php");
	include ("template/sidebar.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<section class="content-header">
		<h1>Edit Tanaman</h1>
		<ol class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li><a href="tanaman.php">Tanaman</a></li>
			<li class="active">Edit Tanaman</li>
		 </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">               
					<?php 
						$kode_tanaman = $_GET['id'];
						$query_mysql = mysql_query("SELECT * FROM tanaman WHERE kode_tanaman='$kode_tanaman'")or die(mysql_error());
						while($data = mysql_fetch_array($query_mysql)){
					?>
					<form name="update" role="form" action="edit_tanamanproses.php" method="post" >
						<div class="box-body">
							<div class="form-group">
								<label>Nama Tanaman</label>
								<input type="hidden" name="kode_tanaman" value="<?php echo $data['kode_tanaman'] ?>">
								<input type="text" class="form-control" name="nama_tanaman" value="<?php echo $data['nama_tanaman'] ?>">
							</div>

							<div class="form-group">
							 	<label>Nama Latin</label>
								<input type="text" class="form-control" name="nama_latin" value="<?php echo $data['nama_latin'] ?>">
							</div>

							<!-- textarea -->
							<div class="form-group">
								<label>Deskripsi</label>
								<textarea class="form-control" rows="3" name="deskripsi" value="<?php echo $data['deskripsi'] ?>"></textarea>
							</div>

							<div class="form-group">
								<label>Jenis Tanaman</label>
								<select class="form-control select2" style="width: 100%;" name="jenis_tanaman">
									<?php 
										$sql = mysql_query('SELECT * FROM jenis_tanaman ORDER BY nama_jenis_tanaman ASC;');
										if (mysql_num_rows($sql)>0) { ?>
											<?php while ($row = mysql_fetch_array($sql)) { ?>
												<option value="<?php echo $row['kode_jenis_tanaman'] ?>"><?php echo $row['nama_jenis_tanaman'] ?></option>}
										<?php   } ?>
									<?php } ?>
								</select>
							</div>
						</div>
						<!-- /.box-body -->	

						<div class="box-footer">
							<button type="reset" class="btn btn-default" ><a href="tanaman.php">Batal</a></button>
							<button type="submit" name="update" value="update" class="btn btn-primary">Update</button>
						</div>
					</form>
					<?php } ?>	
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>