<?php
	include ("../lib/koneksi.php");
	include ("template/head.php");
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pakar') {
    	header("Location: ../");
    }
    else{
	include ("template/topbar.php");
	include ("template/sidebar.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<section class="content-header">
		<h1>Edit Tanda</h1>
		<ol class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li><a href="tanda.php">Tanda</a></li>
			<li class="active">Edit Tanda</li>
	  	</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<?php 
						$id = $_GET['id'];
						$query = mysql_query("SELECT * FROM tanda WHERE kode_tanda='$id'")or die(mysql_error());
						while($data = mysql_fetch_array($query)){
					?>               
					<form name="update" role="form" action="edit_tandaproses.php" method="post" >
						<div class="box-body">
							<!-- text input -->
							<div class="form-group">
								<label>Nama Tanda</label>
								<input type="hidden" name="kode_tanda" value="<?php echo $data['kode_tanda'] ?>">
								<input type="text" class="form-control" name="nama_tanda" value="<?php echo $data['nama_tanda'] ?>">
							</div>

							<!-- textarea -->
							<div class="form-group">
							  <label>Deskripsi</label>
							  <textarea class="form-control" rows="3" name="deskripsi" value="<?php echo $data['deskripsi'] ?>"></textarea>
							</div>
						</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="reset" class="btn btn-default" ><a href="gejala.php">Batal</a></button>
							<button type="submit" name="update" value="update" class="btn btn-primary">Update</button>
						</div>
					</form>
					<?php } ?>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>

<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>