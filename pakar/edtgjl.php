<?php
	include ("../lib/koneksi.php");
	include ("template/head.php");
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pakar') {
    	header("Location: ../");
    }
    else{
	include ("template/topbar.php");
	include ("template/sidebar.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
    	<h1>Edit Aturan Gejala</h1>
    	<ol class="breadcrumb">
        	<li><a href="index.php">Home</a></li>
        	<li><a href="aturan_gejala.php">Aturan Gejala</a></li>
        	<li class="active">Aturan Gejala</li>
      	</ol>
    </section>
    <!-- Main content -->
    <section class="content">
    	<div class="row">
            <div class="col-xs-12">
            	<div class="box">
	                <?php 
						$kode_aturan_gejala = $_GET['id'];
						$query_mysql = mysql_query("SELECT * FROM aturan_gejala WHERE kode_aturan_gejala='$kode_aturan_gejala'")or die(mysql_error());
						while($data = mysql_fetch_array($query_mysql)){
							$kode_gejala = $data['kode_gejala'];
							
					?>
	            	<form name="update" role="form" action="edtgjlproses.php" method="post" enctype="multipart/form-data">
	                <!-- text input -->

			                <div class="form-group">
			                	<label>Foto Gejala</label><br>
			                  	<img src="../gambar/gejala/<?php echo $data['foto']; ?>" width="100"> <br>
		                  		<input type="file" name="gambar" value="<?php echo $data['foto'] ?>">
			                </div>

			            </div>
            			<!-- /.box-body -->    
		                <div class="box-footer">
			                <button type="reset" class="btn btn-default" ><a href="aturan_gejala.php">Batal</a></button>
			                <button type="submit" name="update" value="update" class="btn btn-primary">Update</button>
			            </div>
	            	</form>
	               	<?php } ?>
            	</div>
              	<!-- /.box -->
            </div>
    	</div>
    </section>
    <!-- /.content -->
</div>

<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>