<?php
	include ("../lib/koneksi.php");
	include ("template/head.php");
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pakar') {
    	header("Location: ../");
    }
    else{
	include ("template/topbar.php");
	include ("template/sidebar.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<?php 
		if(isset($_GET['pesan'])){
			$pesan = $_GET['pesan'];
			if($pesan == "input"){
				echo "Data berhasil di input.";
			}else if($pesan == "gagal"){
				echo "Data gagal di input.";
			}else if($pesan == "update"){
				echo "Data berhasil di update.";
			}else if($pesan == "gagalupdate"){
				echo "Data berhasil gagal di update.";
			}else if($pesan == "hapus"){
				echo "Data berhasil di hapus.";
			}else if($pesan == "gagalhapus"){
				echo "Data berhasil gagal di hapus.";
			}
		}
	?>
	<section class="content-header">
		<h1>Data Gejala</h1>
		<ol class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li class="active">Gejala</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="add_gejala.php" class="btn btn-primary">Tambah Gejala</a>  
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="data" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama Gejala</th>
									<th>Deskripsi</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									$no=1;
									$tampil = mysql_query("SELECT * FROM gejala") or die(mysql_error());
									while ($data = mysql_fetch_array($tampil)) {
								?>
								<tr>
									<td><?php echo $no; ?></td>
									<td><?php echo $data['nama_gejala'] ?></td>
									<td><?php echo $data['deskripsi'] ?></td>
									<td> 
										<a href="edit_gejala.php?id=<?php echo $data['kode_gejala']; ?>" class="btn btn-primary btn-sm" > Edit </a> 
										<a href="delete_gejala.php?id=<?php echo $data['kode_gejala']; ?>" class="btn btn-warning btn-sm"> Hapus </a>
									</td>
								</tr>
								<?php
									$no++;
									}
								?>
							</tbody>			
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>