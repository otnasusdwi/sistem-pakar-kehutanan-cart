<?php
	include ("template/head.php");
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pakar') {
    	header("Location: ../");
    }
    else{
	include ("template/topbar.php");
	include ("template/sidebar.php");

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Main content -->
	<section class="content">
	 	<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
				<div style="text-align:left;">		
					<br><b>Selamat Datang dihalaman Administrator</b><br>
					Sistem Pakar Identifikasi 
					Hama & Penyakit Tanaman Kehutanan
				</div>
			</div><!-- /.box-header -->
			<div class="box-body">
				Pilih salah satu menu disamping untuk mengatur website
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</section>
	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>