<?php
	include ("../lib/koneksi.php");
	include ("template/head.php");
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pakar') {
    	header("Location: ../");
    }
    else{
	include ("template/topbar.php");
	include ("template/sidebar.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<?php 
		if(isset($_GET['pesan'])){
			$pesan = $_GET['pesan'];
			if($pesan == "input"){
				echo "Data berhasil di input.";
			}else if($pesan == "gagal"){
				echo "Data gagal di input.";
			}else if($pesan == "update"){
				echo "Data berhasil di update.";
			}else if($pesan == "gagalupdate"){
				echo "Data berhasil gagal di update.";
			}else if($pesan == "hapus"){
				echo "Data berhasil di hapus.";
			}else if($pesan == "gagalhapus"){
				echo "Data berhasil gagal di hapus.";
			}
		}
	?>
	<section class="content-header">
		<h1>Data Pengganggu</h1>
		<ol class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li class="active">Pengganggu</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="add_pengganggu.php" class="btn btn-primary">Tambah Pengganggu</a>  
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="data" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th width="5%">No</th>
									<th width="10%">Nama Pengganggu</th>
									<th width="15%">Nama Latin</th>
									<th width="10%">Foto</th>
									<th width="5%">Kategori</th>
									<th width="20%">Deskripsi</th>
									<th width="20%">Pengendalian</th>
									<th width="15%">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									$no=1;
									$tampil = mysql_query("SELECT * FROM pengganggu ORDER BY kode_pengganggu DESC") or die(mysql_error());
									while ($data = mysql_fetch_array($tampil)) {
										$kode_kategori_pengganggu = $data['kode_kategori_pengganggu'];
										$query = mysql_query("SELECT nama_kategori_pengganggu FROM kategori_pengganggu WHERE kode_kategori_pengganggu = $kode_kategori_pengganggu") or die(mysql_error());
										while ($hasil = mysql_fetch_array($query)) {
								?>
								<tr>
									<td><?php echo $no; ?></td>
									<td><?php echo $data['nama_pengganggu']; ?></td>
									<td><i><?php echo $data['nama_latin']; ?></i></td>
									<td><img class="img-rounded" alt="Cinque Terre" width="152" height="118" img src="../gambar/pengganggu/<?php echo $data['foto']; ?>"></td>
									<td><?php echo $hasil['nama_kategori_pengganggu']; ?></td>
									<td><?php echo $data['deskripsi']; ?></td>
									<td><?php echo $data['pengendalian']; ?></td>
									<td> 
										<a href="edit_pengganggu.php?id=<?php echo $data['kode_pengganggu']; ?>" class="btn btn-primary btn-sm" > Edit </a> 
										<a href="delete_pengganggu.php?id=<?php echo $data['kode_pengganggu']; ?>" class="btn btn-warning btn-sm"> Hapus </a>
									</td>
								</tr>
								<?php 
									}
									$no++;
								}
								?>
								
							</tbody>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>