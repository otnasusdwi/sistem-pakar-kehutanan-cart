<?php
 	include ("../lib/koneksi.php");
	include ("template/head.php");
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pakar') {
    	header("Location: ../");
    }
    else{
	include ("template/topbar.php");
	include ("template/sidebar.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<?php 
		if(isset($_GET['pesan'])){
			$pesan = $_GET['pesan'];
			if($pesan == "input"){
				echo "Data berhasil di input.";
			}else if($pesan == "gagal"){
				echo "Data gagal di input.";
			}else if($pesan == "update"){
				echo "Data berhasil di update.";
			}else if($pesan == "gagalupdate"){
				echo "Data berhasil gagal di update.";
			}else if($pesan == "hapus"){
				echo "Data berhasil di hapus.";
			}else if($pesan == "gagalhapus"){
				echo "Data berhasil gagal di hapus.";
			}
		}
	?>
	<section class="content-header">
		<h1>Data Tanaman</h1>
		<ol class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li class="active">Tanaman</li>
		</ol>
	</section>
	
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="add_tanaman.php" class="btn btn-primary">Tambah Tanaman</a>  
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="data" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama Tanaman</th>
									<th>Nama Latin</th>
									<th>Deskripsi</th>
									<th>Jenis Tanaman</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									$no=1;
									$tampil = mysql_query("SELECT * FROM tanaman") or die(mysql_error());
									while ($data = mysql_fetch_array($tampil)) {
										$kode_jenis_tanaman = $data['kode_jenis_tanaman'];
										$query = mysql_query("SELECT nama_jenis_tanaman FROM jenis_tanaman WHERE kode_jenis_tanaman = $kode_jenis_tanaman") or die(mysql_error());
										while ($hasil = mysql_fetch_array($query)) {
								?>
								<tr>
									<td><?php echo $no; ?></td>
									<td><?php echo $data['nama_tanaman']; ?></td>
									<td><i><?php echo $data['nama_latin']; ?></i></td>
									<td><?php echo $data['deskripsi']; ?></td>
									<td><?php echo $hasil['nama_jenis_tanaman']; ?></td>
									<td> 
										<a href="edit_tanaman.php?id=<?php echo $data['kode_tanaman']; ?>" class="btn btn-primary btn-sm" > Edit </a> 
										<a href="delete_tanaman.php?id=<?php echo $data['kode_tanaman']; ?>" class="btn btn-warning btn-sm"> Hapus </a>
									</td>
								</tr>
									<?php 
									}
									?>
								<?php
									$no++;
								}
								?>
							</tbody>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
  include ("template/js.php");
  include ("template/foot.php");
}
?>