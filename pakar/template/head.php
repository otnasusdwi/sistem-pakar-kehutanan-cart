<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Admin Sistem Pakar</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="../assets/AdminLTE-2.0.5/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="../assets/font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="../assets/ionicons-2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../assets/AdminLTE-2.0.5/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
             folder instead of downloading all of them to reduce the load. -->
        <link href="../assets/AdminLTE-2.0.5/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
        <!-- Select2 -->
        <link href="../assets/AdminLTE-2.0.5/plugins/select2/select2.min.css" rel="stylesheet" >
        <!-- DataTables -->
        <link rel="stylesheet" href="../assets/AdminLTE-2.0.5/plugins/datatables/dataTables.bootstrap.css">
