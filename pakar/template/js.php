
  <footer class="main-footer">
      <div class="pull-right hidden-xs">
          <b>Version</b> 2.0
      </div>
      <strong>Copyright &copy; 2014-2015 <a href="#">Almsaeed Studio</a>.</strong> All rights reserved.
  </footer>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.3 -->
<script src="../assets/AdminLTE-2.0.5/plugins/jQuery/jQuery-2.1.3.min.js"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="../assets/AdminLTE-2.0.5/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="../assets/AdminLTE-2.0.5/plugins/slimScroll/jquery.slimScroll.min.js" type="text/javascript"></script>
<!-- FastClick -->
<script src="../assets/AdminLTE-2.0.5/plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../assets/AdminLTE-2.0.5/dist/js/app.min.js" type="text/javascript"></script>
<script src="../assets/AdminLTE-2.0.5/asset/plugins/morris/morris.min.js" type="text/javascript"></script>
<!-- Select2 -->
<script src="../assets/AdminLTE-2.0.5/plugins/select2/select2.full.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="../assets/AdminLTE-2.0.5/plugins/iCheck/icheck.min.js"></script>
<!-- DataTables -->
<script src="../assets/AdminLTE-2.0.5/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../assets/AdminLTE-2.0.5/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
  $(function () {
    $("#data").DataTable();
    $('#x').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": false
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $('#foto_gjl').on('show.bs.modal', function (e) {
      var idx = $(e.relatedTarget).data('id');
      //menggunakan fungsi ajax untuk pengambilan data
      $.ajax({
        type : 'post',
        url : 'foto_gjl.php',
        data :  'idx='+ idx,
        success : function(data){
        $('.hasil-data').html(data);//menampilkan data ke dalam modal
        }
      });
     });
  });

    $(document).ready(function(){
    $('#foto_tnd').on('show.bs.modal', function (e) {
      var idx = $(e.relatedTarget).data('id');
      //menggunakan fungsi ajax untuk pengambilan data
      $.ajax({
        type : 'post',
        url : 'foto_tnd.php',
        data :  'idx='+ idx,
        success : function(data){
        $('.hasil-data').html(data);//menampilkan data ke dalam modal
        }
      });
     });
  });
</script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
  });
</script>

<script>
  var room = 1;
  function aturangejala() {
    room++;
    var objTo = document.getElementById('aturangejala')
    var divtest = document.createElement("div");
    divtest.setAttribute("class", "form-group removeclass"+room);
    var rdiv = 'removeclass'+room;
    divtest.innerHTML = '<div class="col-sm-3 nopadding"> <div class="form-group"> <select class="form-control select2" id="kode_gejala" name="kode_gejala[]"> <?php $sql = mysql_query('SELECT * FROM gejala ORDER BY nama_gejala ASC;'); if (mysql_num_rows($sql)>0) { ?> <?php while ($row = mysql_fetch_array($sql)) { ?> <option value="<?php echo $row['kode_gejala'] ?>"><?php echo $row['nama_gejala'] ?></option>} <?php } ?> <?php } ?> </select> </div> </div> <div class="col-sm-3 nopadding"> <div class="form-group"> <input type="file"  id="gambar" name="gambar_gejala[]" > </div> </div> <div class="col-sm-3 nopadding"> <div class="form-group"> <input type="text" name="nilai_belief_gejala[]" placeholder="nilai belief gejala 1-100"> </div> </div><div class="col-sm-3 nopadding"> <div class="form-group"> <button class="btn btn-danger" type="button"  onclick="remove_aturangejala('+ room +');"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> Gejala</button></div></div><div class="clear"></div>';
    
    objTo.appendChild(divtest)
  }
  function remove_aturangejala(rid) {
    $('.removeclass'+rid).remove();
  }

  var roomm = 1;
  function aturantanda() {
    roomm++;
    var objTo = document.getElementById('aturantanda')
    var divtest = document.createElement("div");
    divtest.setAttribute("class", "form-group removeclass"+roomm);
    var rdiv = 'removeclass'+roomm;
    divtest.innerHTML = '<div class="col-sm-3 nopadding"> <div class="form-group"> <select class="form-control select2" id="kode_tanda" name="kode_tanda[]"> <?php $sql = mysql_query('SELECT * FROM tanda ORDER BY nama_tanda ASC;'); if (mysql_num_rows($sql)>0) { ?> <?php while ($row = mysql_fetch_array($sql)) { ?> <option value="<?php echo $row['kode_tanda'] ?>"><?php echo $row['nama_tanda'] ?></option>} <?php } ?> <?php } ?> </select> </div> </div> <div class="col-sm-3 nopadding"> <div class="form-group"> <input type="file"  id="gambar" name="gambar_tanda[]" > </div> </div> <div class="col-sm-3 nopadding"> <div class="form-group"><input type="text" name="nilai_belief_tanda[]" placeholder="nilai belief tanda 1-100"> </div> </div><div class="col-sm-3 nopadding"> <div class="form-group"> <button class="btn btn-danger" type="button"  onclick="remove_aturantanda('+ roomm +');"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> Tanda</button></div></div><div class="clear"></div>';
    
    objTo.appendChild(divtest)
  }
  function remove_aturantanda(rid) {
    $('.removeclass'+rid).remove();
  }
</script>