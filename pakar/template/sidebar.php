<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
<!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
  <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="../assets/img/user4-128x128.jpg" class="img-circle" alt="User Image" />
      </div>
      <div class="pull-left info">
        <p>Pakar</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>        
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">Menu</li>
      <li>
        <a href="index.php">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-table"></i>
          <span>Data Umum</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="jenis_tanaman.php"><i class="fa fa-circle-o"></i>Jenis Tanaman</a></li>
          <li><a href="kategori.php"><i class="fa fa-circle-o"></i>Kategori</a></li><li>
          <li><a href="kategori_pengganggu.php"><i class="fa fa-circle-o"></i>Kategori Pengganggu</a></li>
          <li><a href="tanaman.php"><i class="fa fa-circle-o"></i>Tanaman</a></li>
          <li><a href="pengganggu.php"><i class="fa fa-circle-o"></i>Pengganggu</span></a></li>
          <li><a href="gejala.php"><i class="fa fa-circle-o"></i>Gejala</a></li>
          <li><a href="tanda.php"><i class="fa fa-circle-o"></i>Tanda</span></a></li>
        </ul>
      </li>
      <li>
        <a href="kategori_tanaman.php">
          <i class="fa fa-database"></i>
          <span>Kategori Tanaman</span>
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-bug"></i>
          <span>Aturan <i>(Rule Base)</i></span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="aturan_gejala.php"><i class="fa fa-circle-o"></i>Aturan Gejala</a></li>
          <li><a href="aturan_tanda.php"><i class="fa fa-circle-o"></i>Aturan Tanda</a></li><li>
        </ul>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
<!-- =============================================== -->

