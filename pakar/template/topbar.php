</head>
<body class="skin-blue">
    <!-- Site wrapper -->
    <div class="wrapper">
        <header class="main-header">
            <a href="#" class="logo">
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Administrator</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
              <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="../logout.php">Logout <i class="fa fa-lock"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- =============================================== -->