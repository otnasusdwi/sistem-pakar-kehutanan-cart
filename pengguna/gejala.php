<?php
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pengguna') {
    	header("Location: ../");
    }
    else{
	include ("../lib/koneksi.php");
	include ("template/head.php");

	if(isset($_POST['nama_kategori']))  
	{  
		$_SESSION['nama_kategori'] = $_POST['nama_kategori'];  
	}

	$nama = $_SESSION['nama_tanaman'];
	$kategori = $_SESSION['nama_kategori'];
	$sql = mysql_query("SELECT kode_kategori_tanaman FROM kategori_tanaman WHERE kode_tanaman = $nama AND kode_kategori = $kategori")or die(mysql_error());
	while ($data = mysql_fetch_array($sql)) {
		$kode_kategori_tanaman = $data['kode_kategori_tanaman'];
	}
	
?>

<body>
	<nav class="navbar navbar-static-top navbar-inverse">
		<div class="container-fluid">
			<ul class="nav navbar-nav">
				<li><a href="index.php" class="glyphicon glyphicon-home"></a></li>
				<li><a href="jenis_tanaman.php" title="">Jenis Tanaman</a></li>
				<li><a href="nama_tanaman.php" title="">Tanaman</a></li>
				<li><a href="kategori.php" title="">Kategori</a></li>
				<li class="active"><a href="#" title="">Gejala</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
		  		<li><a href="../logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			</ul>
	  	</div>
	</nav>
	
	<section class="content">
		<div class="row">
			<?php
				 $nama=$_SESSION['username'];
				include ("template/sidebar.php");
			?>
			<div class="col-md-10">
				<form role="form" action="tanda.php" method="post" >
					<div class="box">
						<div class="box-header">
							<h2 class="profile-username text-left">Pilih Gejala</h2> 
						</div>
						<!-- /.box-header -->
						<div class="box-body" >
							<table id="data" class="table table-striped table-bordered">
								<tbody>
									<thead>
										<tr>
											<th>Nama Gejala</th>
											<th></th>
										</tr>
									</thead>
							  		<tbody>
										<?php
											$tampil = mysql_query("SELECT * FROM aturan_gejala WHERE kode_kategori_tanaman = $kode_kategori_tanaman") or die(mysql_error());
								  			while ($data = mysql_fetch_array($tampil)) {
								  				$kd = $data['kode_gejala'];
								  				$sql = mysql_query("SELECT nama_gejala FROM gejala WHERE kode_gejala = $kd")or die(mysql_error());
								  				while ($dt = mysql_fetch_array($sql)) {
								  		?>
										<tr>
											<td><input type="checkbox" name="nama_gejala[]" value="<?=$data['kode_gejala']; ?>" class="minimal"> <?php echo $dt['nama_gejala']; ?>
											</td>
											<?php echo "<td><a href='#foto_gejala' class='btn btn-default btn-small' id='custId' data-toggle='modal' data-id=".$data['kode_gejala'].">Foto</a></td>"; ?>
										</tr>
										<?php
											}
								   		?>
										<?php
											}
								   		?>     
									</tbody>
							</table>
							<div class="modal fade" id="foto_gejala" role="dialog">
						        <div class="modal-dialog" role="document">
						            <div class="modal-content">
						                <div class="modal-header">
						                    <button type="button" class="close" data-dismiss="modal">&times;</button>
						                    <h4 class="modal-title">Foto Gejala</h4>
						                </div>
						                <div class="modal-body">
						                    <div class="hasil-data"></div>
						                </div>
						                <div class="modal-footer">
						                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
						                </div>
						            </div>
						        </div>
						    </div>
							<button class="btn btn-default"><a href="kategori.php">Kembali</a></button>
							<button type="submit" name="selanjutnya" value="jenis" class="btn btn-primary">Selanjutnya</button>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</form>
			</div>
		</div>
	</section>

<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>