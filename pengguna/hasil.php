<?php
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pengguna') {
    	header("Location: ../");
    }
    else{
	include ("../lib/koneksi.php");
	include ("template/head.php");


//	echo "<pre>";
	$tampildiagnosa = array();
	$tampildiagnosa = $_SESSION["tampil"];
//	print_r($tampildiagnosa);
//	exit();

?>

<body>
	<nav class="navbar navbar-static-top navbar-inverse">
		<div class="container-fluid">
			<ul class="nav navbar-nav">
				<li><a href="index.php" class="glyphicon glyphicon-home"></a></li>
				<li><a href="jenis_tanaman.php" title="">Jenis Tanaman</a></li>
				<li><a href="nama_tanaman.php" title="">Tanaman</a></li>
				<li><a href="kategori.php" title="">Kategori</a></li>
				<li><a href="gejala.php" title="">Gejala</a></li>
				<li><a href="tanda.php" title="">Tanda</a></li>
				<li class="active"><a href="#" title="">Hasil Diagnosa</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
		  		<li><a href="../logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			</ul>
	  	</div>
	</nav>
	
	<section class="content">
		<div class="row">
			<?php
				include ("template/sidebar.php");
			?>
			<div class="col-md-10">
				<form role="form" action="diagnosa_proses.php" method="post" >
					<div class="box">
						<div class="box-header">
							<p style="text-align:justify;">
								<img src="../assets/img/notepad-2.png" width="100" height="100" style="float:left; margin:0 20px 3px 0;" />
								<h2><b>Hasil Diagnosa</b></h2> 
								<h4>Kemungkinan Hama / Penyakit</h4>
							</p>
						</div>
						<!-- /.box-header -->
						<div class="box-body" >
							<table id="data" class="table table-striped table-bordered">
								<tbody>
									<thead>
										<tr>
											<th>No</th>
											<th>Nama Pengganggu</th>
											<th></th>
										</tr>
									</thead>
							  		<tbody>
									  	<?php 
											for ($i=0; $i < count($tampildiagnosa); $i++) {
												$kode = $tampildiagnosa[$i];
												$sql = mysql_query("SELECT * FROM pengganggu WHERE kode_pengganggu = $kode")or die(mysql_error());
												while ($data = mysql_fetch_array($sql)) {												
										?>
										<tr>
											<td><?php echo $i+1 ?></td>
											<td><?php echo $data['nama_pengganggu'] ?></td>
											<?php echo "<td><a href='#detail' class='btn btn-default btn-small' data-toggle='modal' data-id=".$data['kode_pengganggu'].">Detail</a></td>"; ?>
										</tr>
									  	
									  	<?php 
												}
											}
										?>
									</tbody>
							</table>
							<div class="modal fade" id="detail" role="dialog">
						        <div class="modal-dialog" role="document">
						            <div class="modal-content">
						                <div class="modal-header">
						                    <button type="button" class="close" data-dismiss="modal">&times;</button>
						                    <h4 class="modal-title">Detail Pengganggu</h4>
						                </div>
						                <div class="modal-body">
						                    <div class="hasil-data"></div>
						                </div>
						                <div class="modal-footer">
						                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
						                </div>
						            </div>
						        </div>
						    </div>
							<br>
							<a href="index.php" class="btn btn-primary" >Diagnosa Lagi</a>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</form>
			</div>
		</div>
	</section>

<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>