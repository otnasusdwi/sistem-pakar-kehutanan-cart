<?php
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pengguna') {
    	header("Location: ../");
    }
    else{
	include ("../lib/koneksi.php");
	include ("template/head.php");
?>

<body>
	<nav class="navbar navbar-static-top navbar-inverse">
		<div class="container-fluid">
			<ul class="nav navbar-nav">
				<li class="active"><a href="#" class="glyphicon glyphicon-home"></a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="../logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			</ul>
		</div>
	</nav>
	
	<section class="content">
		<div class="row">
			<?php
				include ("template/sidebar.php");
			?>
			<div class="col-md-10">
				<div class="box">
					<div class="box-header">
						<h2 class="profile-username text-left">Histori Pengguna</h2> 
					</div>
						<!-- /.box-header -->
					<div class="box-body">
						<div class="box-body table-responsive no-padding">
							<table class="table table-hover">
								<tr>
									<th>No</th>
									<th>Tanggal</th>
									<th>Waktu</th>
									<th>Gejala</th>
									<th>Tanda</th>
									<th>Pengganggu</th>
								</tr>
								<?php
									$i = 1;
									$sql = mysql_query("SELECT * FROM histori WHERE kode_user = '$nama' ORDER BY kode_histori DESC")or die(mysql_error());
									while ($data = mysql_fetch_array($sql)) {
									?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $data['tanggal']; ?></td>
									<td><?php echo $data['waktu']; ?></td>
									<td><?php echo $data['kode_gejala']; ?></td>
									<td><?php echo $data['kode_tanda']; ?></td>
									<td><?php echo $data['kode_pengganggu']; ?></td>
									
								</tr>
								<?php
										$i++;

									}
								 ?>
						 	</table>
						  	<div class="modal fade" id="detail_histori" role="dialog">
						        <div class="modal-dialog" role="document">
						            <div class="modal-content">
						                <div class="modal-header">
						                    <button type="button" class="close" data-dismiss="modal">&times;</button>
						                    <h4 class="modal-title">Detail Penyakit</h4>
						                </div>
						                <div class="modal-body">
						                    <div class="hasil-data"></div>
						                </div>
						                <div class="modal-footer">
						                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
						                </div>
						            </div>
						        </div>
						    </div>
						</div>
						<!-- /.box-body -->
				</div><!-- /.box -->
			</div>
		</div>
	</section>
<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>