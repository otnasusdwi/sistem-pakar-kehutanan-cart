<?php
	include ("template/head.php");
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pengguna') {
    	header("Location: ../");
    }
    else{

?>

<body>
	<nav class="navbar navbar-static-top navbar-inverse">
		<div class="container-fluid">
			<ul class="nav navbar-nav">
		  		<li class="active"><a href="#" class="glyphicon glyphicon-home"></a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
		  		<li><a href="../logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			</ul>
	  	</div>
	</nav>
	
	<section class="content">
		<div class="row">
			<?php
				include ("template/sidebar.php");
			?>
			<div class="col-md-10">
				<div class="box">
					<div class="box-header with-border">
						<div class="box-title">
							<p style="text-align:justify;">
							<img src="../assets/img/network.png" width="150" height="150" style="float:left; margin:0 9px 3px 0;" />
							<h2><b>Selamat Datang di Sistem Pakar <br>Identifikasi Penyakit / Hama Tanaman Kehutanan</b></h2>
							</p>
						</div>
					</div><!-- /.box-header -->
					<div class="box-body">
						<h3><b>Tentang Aplikasi :</b></h3>
						<p >
							<h4 style="text-align:justify;">Website sistem pakar ini bertujuan untuk membantu pengguna dalam mendiagnosa penyakit pada tanaman kehutanan. Untuk melakukan proses diagnosa, ada beberapa tahapan yaitu, memilih jenis tanaman, tanaman, kategori tanaman, gejala, dan tanda yang ada dilapangan. Setelah itu sistem akan menampilkan hasil diagnosa penyakit pada tanaman serta pengendaliannya. Untuk melakukan diagnosa, klik tombol Mulai Diagnosa dibawah ini.</h4>
							
						</p>
					</div><!-- /.box-body -->
					<div class="box-footer" style="text-align:center">
						<a href="jenis_tanaman.php" class="btn btn-primary">Mulai Diagnosa</a>
					</div><!-- box-footer -->
				</div><!-- /.box -->
			</div>
		</div>
	</section>
<?php
	include ("template/js.php");
	include ("template/foot.php");
	}
?>