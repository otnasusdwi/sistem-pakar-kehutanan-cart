<?php
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pengguna') {
    	header("Location: ../");
    }
    else{

	include ("../lib/koneksi.php");
	include ("template/head.php");

?>

<body>
	<nav class="navbar navbar-static-top navbar-inverse">
	  <div class="container-fluid">
		<ul class="nav navbar-nav">
		  <li><a href="index.php" class="glyphicon glyphicon-home"></a></li>
		  <li class="active"><a href="#" title="">Jenis Tanaman</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
		  <li><a href="../logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
		</ul>
	  </div>
	</nav>
	
	<section class="content">
		<div class="row">
			<?php
				include ("template/sidebar.php");
			?>
			<div class="col-md-10">
				<form role="form" action="nama_tanaman.php" method="post" >
					<div class="box">
						<div class="box-header">
							<h2 class="profile-username text-left">Pilih Jenis Tanaman</h2> 
						</div>
						<!-- /.box-header -->
						<div class="box-body" >
							<table id="data" class="table table-striped table-bordered">
								<tbody>
									<?php
										$tampil = mysql_query("SELECT * FROM jenis_tanaman") or die(mysql_error());
								  		while ($data = mysql_fetch_array($tampil)) {
										?>
										<tr>
											<td>
												<label for="jenis_tanaman">
													<input required type="radio" name="jenis_tanaman" value="<?=$data['kode_jenis_tanaman']; ?>" class="minimal"> <?php echo $data['nama_jenis_tanaman']; ?>
												</label>
											</td>											
										</tr>
										<?php
										}
									?>
								</tbody>
							</table>
							<br>
							<button onclick="window.location.reload(history.back());" class="btn btn-default" >Batal</button>
							<button type="submit" name="selanjutnya" value="jenis" class="btn btn-primary">Selanjutnya</button>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</form>
			</div>
		</div>
	</section>

<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>