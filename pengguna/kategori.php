<?php
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pengguna') {
    	header("Location: ../");
    }
    else{
	include ("../lib/koneksi.php");
	include ("template/head.php");

	if(isset($_POST['nama_tanaman']))  
	{  
		$_SESSION['nama_tanaman'] = $_POST['nama_tanaman'];   

	}  
?>

<body>
	<nav class="navbar navbar-static-top navbar-inverse">
		<div class="container-fluid">
			<ul class="nav navbar-nav">
				<li><a href="index.php" class="glyphicon glyphicon-home"></a></li>
				<li><a href="jenis_tanaman.php" title="">Jenis Tanaman</a></li>
				<li><a href="nama_tanaman.php" title="">Tanaman</a></li>
				<li class="active"><a href="#" title="">Kategori</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
		  		<li><a href="../logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			</ul>
	  	</div>
	</nav>
	
	<section class="content">
		<div class="row">
			<?php
				include ("template/sidebar.php");
			?>
			<div class="col-md-10">
				<form role="form" action="gejala.php" method="post" >
					<div class="box">
						<div class="box-header">
							<h2 class="profile-username text-left">Pilih Kategori Tanaman</h2> 
						</div>
						<!-- /.box-header -->
						<div class="box-body" >
							<table id="data" class="table table-striped table-bordered">
								<tbody>
									<!-- radio -->
									<?php 
								  		$tampil = mysql_query("SELECT * FROM kategori") or die(mysql_error());
								  		while ($data = mysql_fetch_array($tampil)) {
										?>
										<tr>
											<td>
												<input required type="radio" name="nama_kategori" value="<?=$data['kode_kategori']; ?>" class="minimal">
									  			<?php echo $data['nama_kategori']; ?>
									  		</td>
									  		<?php echo "<td><a href='#foto_kategori' class='btn btn-default btn-small' data-toggle='modal' data-id=".$data['kode_kategori'].">Foto</a></td>"; ?>
										</tr>
										<?php
											}
									   	?>
									</tbody>
							</table>
							<div class="modal fade" id="foto_kategori" role="dialog">
						        <div class="modal-dialog" role="document">
						            <div class="modal-content">
						                <div class="modal-header">
						                    <button type="button" class="close" data-dismiss="modal">&times;</button>
						                    <h4 class="modal-title">Foto Tanaman</h4>
						                </div>
						                <div class="modal-body">
						                    <div class="hasil-data"></div>
						                </div>
						                <div class="modal-footer">
						                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
						                </div>
						            </div>
						        </div>
						    </div>
							<br>
							<button class="btn btn-default"><a href="nama_tanaman.php">Kembali</a></button>
							<button type="submit" name="selanjutnya" value="jenis" class="btn btn-primary">Selanjutnya</button>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</form>
			</div>
		</div>
	</section>

<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>