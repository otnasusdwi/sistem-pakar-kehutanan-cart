<?php
	include ("template/head.php");
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pengguna') {
    	header("Location: ../");
    }
    else{
?>

<body>
	<nav class="navbar navbar-static-top navbar-inverse">
		<div class="container-fluid">
			<ul class="nav navbar-nav">
		  		<li class="active"><a href="#" class="glyphicon glyphicon-home"></a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
		  		<li><a href="../logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			</ul>
	  	</div>
	</nav>
	
	<section class="content">
		<div class="row">
			<?php
				include ("template/sidebar.php");
			?>
			<div class="col-md-10">
				<div class="box">
					<div class="box-header with-border">
						<div class="box-title">
							<p>
								Kontak Kami
							</p>
						</div>
					</div><!-- /.box-header -->
					<div class="box-body">
						<p style="text-align:justify">
							<h4><b>Jika anda memiliki kritik dan saran, anda bisa menghubungi kami di : <br /></b><br />
							<table>
								<tbody>
									<tr>
										<td><b>Email</b></td>
										<td> : </td>
										<td>smwidyastuti@ugm.ac.id</td>
									</tr>
									<tr>
										<td><b>Alamat Instansi</b></td>
										<td> : </td>
										<td>Laboraturium Perlindungan dan Kesehatan Hutan, Fakultas Kehutanan, UGM</td>
									</tr>
								</tbody>
							</table>							
						</p>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div>
		</div>
	</section>
<?php
	include ("template/js.php");
	include ("template/foot.php");
	}
?>