<?php
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pengguna') {
    	header("Location: ../");
    }
    else{

	include ("../lib/koneksi.php");
	include ("template/head.php");

	if(isset($_POST['jenis_tanaman']))  
	{  
		$_SESSION['jenis_tanaman'] = $_POST['jenis_tanaman'];  
	}  

?>

<body>
	<nav class="navbar navbar-static-top navbar-inverse">
		<div class="container-fluid">
			<ul class="nav navbar-nav">
				<li><a href="index.php" class="glyphicon glyphicon-home"></a></li>
				<li><a href="jenis_tanaman.php" title="">Jenis Tanaman</a></li>
				<li class="active"><a href="#" title="">Tanaman</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
		  		<li><a href="../logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			</ul>
	  	</div>
	</nav>
	
	<section class="content">
		<div class="row">
			<?php
				include ("template/sidebar.php");
			?>
			<div class="col-md-10">
				<form role="form" action="kategori.php" method="post" >
					<div class="box">
						<div class="box-header">
							<h2 class="profile-username text-left">Pilih Nama Tanaman</h2> 
						</div>
						<!-- /.box-header -->
						<div class="box-body" >
							<table id="data" class="table table-striped table-bordered">
								<thead>
									<th>Nama Tanaman</th>
									<th><i>Nama Latin</i></th>
								</thead>
								<tbody>
									<?php 
								  		$tampil = mysql_query("SELECT * FROM tanaman ") or die(mysql_error());
								  		while ($data = mysql_fetch_array($tampil)) {
									?>
									<tr>
										<td>
											<input required type="radio" name="nama_tanaman" value="<?=$data['kode_tanaman']; ?>" class="minimal">
								  			<?php echo $data['nama_tanaman']; ?>
								  		</td>
								  		<td><i><?php echo $data['nama_latin']; ?></i></td>
									</tr>
									<?php
										}
								   	?>
								</tbody>
							</table>
							<br>
							<button class="btn btn-default" ><a href="jenis_tanaman.php">Kembali</a></button>
							<button type="submit" name="selanjutnya" value="jenis" class="btn btn-primary">Selanjutnya</button>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</form>
			</div>
		</div>
	</section>

<?php
	include ("template/js.php");
	include ("template/foot.php");
}
?>