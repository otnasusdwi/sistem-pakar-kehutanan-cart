<?php
	session_start();
    $nama=$_SESSION['username'];
    $level=$_SESSION['level'];

    if (empty($nama) or $level != 'pengguna') {
    	header("Location: ../");
    }
    else{
	include ("../lib/koneksi.php");
	include ("template/head.php");

	$tampung_gejala=count($_POST['nama_gejala']);

	for ($i=0; $i < $tampung_gejala; $i++) { 
		$nama_gejala[]=$_POST['nama_gejala'][$i];
	}

	$_SESSION['nama_gejala'] = $nama_gejala;
	$nama = $_SESSION['nama_tanaman'];
	$kategori = $_SESSION['nama_kategori'];
	$sql = mysql_query("SELECT kode_kategori_tanaman FROM kategori_tanaman WHERE kode_tanaman = $nama AND kode_kategori = $kategori")or die(mysql_error());
	while ($data = mysql_fetch_array($sql)) {
		$kode_kategori_tanaman = $data['kode_kategori_tanaman'];
	}



?>

<body>
	<nav class="navbar navbar-static-top navbar-inverse">
		<div class="container-fluid">
			<ul class="nav navbar-nav">
				<li><a href="index.php" class="glyphicon glyphicon-home"></a></li>
				<li><a href="jenis_tanaman.php" title="">Jenis Tanaman</a></li>
				<li><a href="nama_tanaman.php" title="">Tanaman</a></li>
				<li><a href="kategori.php" title="">Kategori</a></li>
				<li><a href="gejala.php" title="">Gejala</a></li>
				<li class="active"><a href="#" title="">Tanda</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
		  		<li><a href="../logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			</ul>
	  	</div>
	</nav>
	
	<section class="content">
		<div class="row">
			<?php
			$nama=$_SESSION['username'];
				include ("template/sidebar.php");
			?>
			<div class="col-md-10">
				<form role="form" action="diagnosa_proses.php" method="post" >
					<div class="box">
						<div class="box-header">
							<h2 class="profile-username text-left">Pilih Tanda</h2> 
						</div>
						<!-- /.box-header -->
						<div class="box-body" >
							<table id="data" class="table table-striped table-bordered">
								<tbody>
									<thead>
										<tr>
											<th>Nama Tanda</th>
											<th></th>
										</tr>
									</thead>
							  		<tbody>
										<?php
											$tampil = mysql_query("SELECT * FROM aturan_tanda WHERE kode_kategori_tanaman = $kode_kategori_tanaman") or die(mysql_error());
								  			while ($data = mysql_fetch_array($tampil)) {
								  				$kd = $data['kode_tanda'];
								  				$sql = mysql_query("SELECT nama_tanda FROM tanda WHERE kode_tanda = $kd")or die(mysql_error());
								  				while ($dt = mysql_fetch_array($sql)) {
								  		?>
										<tr>
											<td><input type="checkbox" name="nama_tanda[]" value="<?=$data['kode_tanda']; ?>" class="minimal"> <?php echo $dt['nama_tanda']; ?>
											</td>
											<?php echo "<td><a href='#foto_tanda' class='btn btn-default btn-small' id='custId' data-toggle='modal' data-id=".$data['kode_tanda'].">Foto</a></td>"; ?>
										</tr>
										<?php
											}
									   	?> 
									   	<?php
											}
									   	?>     
									</tbody>
							</table>
							<div class="modal fade" id="foto_tanda" role="dialog">
						        <div class="modal-dialog" role="document">
						            <div class="modal-content">
						                <div class="modal-header">
						                    <button type="button" class="close" data-dismiss="modal">&times;</button>
						                    <h4 class="modal-title">Foto Tanda</h4>
						                </div>
						                <div class="modal-body">
						                    <div class="hasil-data"></div>
						                </div>
						                <div class="modal-footer">
						                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
						                </div>
						            </div>
						        </div>
						    </div>
							<br>
							<button class="btn btn-default"><a href="gejala.php">Kembali</a></button>
							<button type="submit" name="selanjutnya" value="tanda" class="btn btn-primary">Diagnosa</button>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</form>
			</div>
		</div>
	</section>

<?php

	include ("template/js.php");
	include ("template/foot.php");
}
?>