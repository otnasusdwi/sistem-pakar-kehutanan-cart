<!DOCTYPE html>
<html>
<head>
	<title>Makitanweb</title>
	<!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../assets/AdminLTE-2.0.5/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../assets/font-awesome-4.3.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../assets/ionicons-2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/dist/css/AdminLTE.min.css">
   <!-- Select2 -->
  <link rel="stylesheet" href="../assets/AdminLTE-2.0.5/plugins/select2/select2.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="../assets/AdminLTE-2.0.5/plugins/iCheck/all.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../assets/AdminLTE-2.0.5/plugins/datatables/dataTables.bootstrap.css">
  <style>
		body {
		    background-color:#ecf0f5;
		}
	</style>

</head>