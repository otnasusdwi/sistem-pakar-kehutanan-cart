<!-- jQuery 2.1.3 -->
<script src="../assets/AdminLTE-2.0.5/plugins/jQuery/jQuery-2.1.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../assets/AdminLTE-2.0.5/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../assets/AdminLTE-2.0.5/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../assets/dist/js/app.min.js"></script>
<!-- Select2 -->
<script src="../assets/AdminLTE-2.0.5/plugins/select2/select2.full.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="../assets/AdminLTE-2.0.5/plugins/iCheck/icheck.min.js"></script>
<!-- DataTables -->
<script src="../assets/AdminLTE-2.0.5/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../assets/AdminLTE-2.0.5/plugins/datatables/dataTables.bootstrap.min.js"></script>



<script>
  $(function () {
	//Initialize Select2 Elements
	$(".select2").select2();

	//iCheck for checkbox and radio inputs
	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
	  checkboxClass: 'icheckbox_minimal-blue',
	  radioClass: 'iradio_minimal-blue'
	});
  });
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#detail').on('show.bs.modal', function (e) {
			var idx = $(e.relatedTarget).data('id');
			//menggunakan fungsi ajax untuk pengambilan data
			$.ajax({
				type : 'post',
				url : 'detail_pengganggu.php',
				data :  'idx='+ idx,
				success : function(data){
				$('.hasil-data').html(data);//menampilkan data ke dalam modal
				}
			});
		 });
	});

	$(document).ready(function(){
		$('#foto_kategori').on('show.bs.modal', function (e) {
			var idx = $(e.relatedTarget).data('id');
			//menggunakan fungsi ajax untuk pengambilan data
			$.ajax({
				type : 'post',
				url : 'foto_kategori.php',
				data :  'idx='+ idx,
				success : function(data){
				$('.hasil-data').html(data);//menampilkan data ke dalam modal
				}
			});
		 });
	});

	$(document).ready(function(){
		$('#foto_gejala').on('show.bs.modal', function (e) {
			var idx = $(e.relatedTarget).data('id');
			//menggunakan fungsi ajax untuk pengambilan data
			$.ajax({
				type : 'post',
				url : 'foto_gejala.php',
				data :  'idx='+ idx,
				success : function(data){
				$('.hasil-data').html(data);//menampilkan data ke dalam modal
				}
			});
		 });
	});

	$(document).ready(function(){
		$('#foto_tanda').on('show.bs.modal', function (e) {
			var idx = $(e.relatedTarget).data('id');
			//menggunakan fungsi ajax untuk pengambilan data
			$.ajax({
				type : 'post',
				url : 'foto_tanda.php',
				data :  'idx='+ idx,
				success : function(data){
				$('.hasil-data').html(data);//menampilkan data ke dalam modal
				}
			});
		 });
	});

	$(document).ready(function(){
		$('#detail_histori').on('show.bs.modal', function (e) {
			var idx = $(e.relatedTarget).data('id');
			//menggunakan fungsi ajax untuk pengambilan data
			$.ajax({
				type : 'post',
				url : 'detail_histori.php',
				data :  'idx='+ idx,
				success : function(data){
				$('.hasil-data').html(data);//menampilkan data ke dalam modal
				}
			});
		 });
	});
</script>